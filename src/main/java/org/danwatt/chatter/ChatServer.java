package org.danwatt.chatter;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class ChatServer {

	public List<String> lines;
	public int currentLine = 0;
	public long startTime;

	@SuppressWarnings("restriction")
	public void run(String port, String delayInSeconds) throws IOException {
		lines = IOUtils.readLines(ChatServer.class.getResourceAsStream("script"));

		InetSocketAddress addr = new InetSocketAddress(Integer.parseInt(port));

		HttpServer server = HttpServer.create(addr, 0);

		server.createContext("/", new MyHandler(this));
		ExecutorService tp = Executors.newCachedThreadPool();
		server.setExecutor(tp);
		server.start();
		System.out.println("Server is listening on port " + port);
		startTime = System.currentTimeMillis() + 1000 * Integer.parseInt(delayInSeconds);
		System.out.println("Starting in " + ((startTime - System.currentTimeMillis()) / 1000) + " seconds");
	}

}

@SuppressWarnings("restriction")
class MyHandler implements HttpHandler {
	private final ChatServer chatServer;

	public MyHandler(ChatServer chatServer) {
		this.chatServer = chatServer;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String requestMethod = exchange.getRequestMethod();
		String url = exchange.getRequestURI().toASCIIString();
		System.out.println(requestMethod + ": " + url);
		String client = url.substring(1);
		if ("GET".equalsIgnoreCase(requestMethod)) {
			Headers responseHeaders = exchange.getResponseHeaders();
			responseHeaders.set("Content-Type", "text/plain");
			if (chatServer.currentLine >= chatServer.lines.size()) {
				exchange.sendResponseHeaders(404, 0);
			} else {
				exchange.sendResponseHeaders(200, 0);
			}
			String body = "";
			while (true) {
				if (System.currentTimeMillis() < chatServer.startTime) {
					System.out.println("Client " + client + " is waiting for start time");
					sleep10();
					continue;
				}
				if (chatServer.lines.get(chatServer.currentLine).startsWith(client + ":")) {
					break;
				} else {
					sleep10();
				}
			}
			if ("".equals(client)) {
				body = "" + StringUtils.join(chatServer.lines, "\n");
			} else {
				body = StringUtils.substringAfter(chatServer.lines.get(chatServer.currentLine), ":");
			}

			OutputStream responseBody = exchange.getResponseBody();
			responseBody.write(body.getBytes());
			responseBody.close();
		} else if ("DELETE".equalsIgnoreCase(requestMethod)) {
			exchange.sendResponseHeaders(200, 0);
			exchange.getResponseBody().close();
			chatServer.currentLine++;
		}
	}

	private void sleep10() {
		try {
			Thread.sleep(250L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}