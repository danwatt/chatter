package org.danwatt.chatter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class ChatClient {
	private static Map<String, String> voices = new HashMap<String, String>();
	static {
		voices.put("A", "Alex");
		voices.put("B", "Alex");
		ScheduledExecutorService se = Executors.newScheduledThreadPool(1);
		se.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				try {
					Runtime.getRuntime().exec(new String[] { "osascript", "-e", "set volume 5" });
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}, 0, 1, TimeUnit.SECONDS);
	}


	public void run(String serverAddress, String who) {
		DefaultHttpClient c = new DefaultHttpClient();
		String url = "http://" + serverAddress + "/" + who;
		try {
			while (true) {
				HttpGet get = new HttpGet(url);
				HttpDelete del = new HttpDelete(url);
				HttpResponse response = c.execute(get);
				if (response.getStatusLine().getStatusCode() == 404) {
					break;
				}
				String body = IOUtils.toString(response.getEntity().getContent());
				System.out.println(body);
				Process tr = say(body, voices.get(who));
				c.execute(del);
				get.abort();
				del.abort();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Process say(String body, String voice) throws IOException, InterruptedException {
		Process tr = Runtime.getRuntime().exec(new String[] { "say", "-v", voice, "-r", "190", body });
		System.out.println("Status: " + tr.waitFor());
		return tr;
	}

}
