package org.danwatt.chatter;

public class Main {
	public static void main(String[] args) throws Exception {
		String mode = args[0];
		if ("server".equalsIgnoreCase(mode)) {
			new ChatServer().run(args[1], args[2]);
		} else {
			new ChatClient().run(args[1], args[2]);
		}

	}
}